# Stage 0, based on Node.js, to build and compile Angular
FROM node:9.11.1 as node
WORKDIR /app
COPY package.json /app/
RUN npm install
COPY ./ /app/
RUN npm run build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:alpine
RUN rm -rf /usr/share/nginx/html/*
RUN rm -rf /etc/nginx/conf.d/*
RUN rm -rf /etc/nginx/sites-available/*
COPY --from=node /app/dist/ /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/nginx.conf

COPY launch.sh /
RUN chmod +x launch.sh
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

