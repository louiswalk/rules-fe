import {APP_BASE_HREF, DatePipe} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CoreModule} from './@core/core.module';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ThemeModule} from './@theme/theme.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {DataModule} from './pages/rules/data/data.module';
import {FormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {AuthModule} from './auth/auth.module';
import {AuthInterceptor} from './auth/auth.interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    Ng2SmartTableModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    ToasterModule.forChild(),
    DataModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    AuthModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: APP_BASE_HREF, useValue: '/',
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    ToasterService,
    DatePipe,
  ],
  entryComponents: [],
})
export class AppModule {
}
