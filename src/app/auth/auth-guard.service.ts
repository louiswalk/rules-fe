import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {AuthService} from './auth.service';
import {environment} from '../../environments/environment';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService) {
  }

  canActivate(): boolean {
    const urlEncoded = encodeURI(environment.authRedirectUrl);
    const authUrl = 'https://auth.vatit.com';
    if (!this.authService.isAuthenticated()) {
      window.location.href = authUrl + '/login?client-id=' + environment.authClientId  + '&redirect-uri=' + urlEncoded;
      return false;
    }
    return true;
  }
}
