import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LocationStrategy} from '@angular/common';
import qs from 'query-string'
import {environment} from '../../environments/environment';

interface AuthResponse {
  accessToken: string;
  userDetails: UserDetailsResponse;
}
export interface UserDetailsResponse {
  auth0AuthzAuthPolicyResponse: Auth0PermissionsResponse;
  username: string;
  email: string;
  pictureUrl: string;
  fullname: string;
}
interface Auth0PermissionsResponse {
  groups: string[];
  permissions: string[];
  roles: string[]
}

@Component({
  selector: 'ngx-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  access_token: string;
  sepAce: boolean;

  constructor(private http: HttpClient,
              private route: ActivatedRoute,
              private router: Router,
              private url: LocationStrategy) {
    switch (this.url.path()) {
      case '/token-exchange':
        this.exchangeToken();
        break;

      case '/logout':
        this.logout();
        break;

      default:
        break;
    }
  }

  ngOnInit() {
  }

  public exchangeToken() {
    if (this.router.url.indexOf('#') !== -1) {
      const fragment = qs.parse(this.router.parseUrl(this.router.url).fragment);
      this.access_token = fragment.access_token;
    } else {
      this.route.queryParams.subscribe((params: Params) => {
        this.access_token = params['access_token'];
      });
    }

    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Accept': 'application/json',
    });

    this.http.post<AuthResponse>(environment.authUrl + 'security/api/token/exchange',
      {
        access_token: this.access_token,
        token_as_cookie: false,
        separate_ase_token: true,
        token_as_payload: true,
        get_login_details: true,
      },
      {
        headers: headers,
      }).subscribe(
      response => {
        localStorage.setItem('accessToken', response.accessToken);
        localStorage.setItem('userDetails', JSON.stringify(response.userDetails));
        this.router.navigate(['']);
      },
      err => {
        console.log('Error occurred:');
        console.log(err);
      },
    );
  }

  public logout() {
    // let urlEncoded = encodeURI(environment.authLogoutRedirectUrl);
    // const authLogoutUrl = `${environment.authUrl}/v2/logout?client_id=${environment.authClientId}&redirectTo=${urlEncoded}`

    localStorage.removeItem('accessToken');

    // window.location.href = authLogoutUrl;
    this.router.navigate(['']);
  }

}
