import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthService} from './auth.service';
import {AuthGuard} from './auth-guard.service';
import {authComponent, AuthRoutingModule} from './auth-routing.module';

@NgModule({
  declarations: [authComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
  ],
  providers: [
    AuthGuard,
    AuthService,
  ],
})
export class AuthModule {
}
