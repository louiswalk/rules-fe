import {Injectable} from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthService {

  constructor(public jwtHelper: JwtHelperService) {
  }

  getToken(): string {
    return localStorage.getItem('accessToken');
  }
  getUserDetails(): string {
    return localStorage.getItem('userDetails')
  }

  isAuthenticated() {
    const jwt = this.getToken();
    return !this.jwtHelper.isTokenExpired(jwt);
  }

}
