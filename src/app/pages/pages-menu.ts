import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'VAT RULES',
    group: true,
  },
  {
    title: 'Document Type',
    icon: 'nb-keypad',
    link: '/pages/rules',
    children: [
      {
        title: 'Claim Document',
        link: '/pages/rules/du-groups',
      },
      {
        title: 'Supporting Document',
        link: '/pages/rules/supporting-documents',
      },
      {
        title: 'Vat Scheme Document',
        link: '/pages/rules/vat-scheme-supporting-documents',
      },
    ],
  },
  {
    title: 'Vat Scheme',
    icon: 'ion-wrench',
    link: '/pages/rules',
    children: [
      {
        title: 'VAT Authorities',
        link: '/pages/rules/vat-authorities',
      },
      {
        title: 'Schemes',
        link: '/pages/rules/vat-schemes',
      },
      {
        title: 'Types',
        link: '/pages/rules/vat-scheme-types',
      },
      {
        title: 'Expense Agreements',
        link: '/pages/rules/vat-scheme-expense-agreements',
      },
      {
        title: 'Expense Types',
        link: '/pages/rules/vat-scheme-expense-types',
      },
      {
        title: 'Periods',
        link: '/pages/rules/vat-scheme-periods',
      },
      {
        title: 'Period Descriptions',
        link: '/pages/rules/vat-scheme-period-descriptions',
      },
      {
        title: 'Period Types',
        link: '/pages/rules/vat-scheme-period-types',
      },
      {
        title: 'Lookup Item Codes',
        link: '/pages/rules/vat-rule-lookup-items',
      },
      {
        title: 'Lookup Categories',
        link: '/pages/rules/vat-rule-lookup-categories',
      },
      {
        title: 'Invoice Statuses',
        link: '/pages/rules/invoice-statuses',
      },
    ],
  },
  {
    title: 'Expense',
    icon: 'ion-funnel',
    link: '/pages/rules',
    children: [

      {
        title: 'Types',
        link: '/pages/rules/expense-types',
      },
      {
        title: 'Vat Rates',
        link: '/pages/rules/expense-agreement-vat-rates',
      },
      {
        title: 'Categories',
        link: '/pages/rules/expense-type-categories',
      },
      {
        title: 'Codes',
        link: '/pages/rules/expense-type-codes',
      },
      {
        title: 'Sub Codes',
        link: '/pages/rules/expense-type-sub-codes',
      },
    ],
  },
  {
    title: 'Settings',
    icon: 'nb-gear',
    link: '/pages/rules',
    children: [
      {
        title: 'No Reciprocity',
        link: '/pages/rules/reciprocity',
      },
      {
        title: 'Countries',
        link: '/pages/rules/countries',
      },
      {
        title: 'Country Groups',
        link: '/pages/rules/country-groups',
      },
      {
        title: 'Currencies',
        link: '/pages/rules/currencies',
      },
    ],
  },
];
