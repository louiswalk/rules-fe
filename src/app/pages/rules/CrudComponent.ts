import {Injectable} from '@angular/core';

@Injectable()
export abstract class CrudComponent {
  service: any;
  config: any;
  settings: any;
  source: any;


  abstract getEntities(): void;

  abstract updateEntity(entity): void;

  abstract addEntity(entity): void;

  abstract deleteEntity(entity): void;
}
