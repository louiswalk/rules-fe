import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export abstract class CrudService {
  entityURL: string;

  abstract getEntities(): Observable<any>;

  abstract updateEntity(entity): Observable<any>;

  abstract addEntity(entity): Observable<any>;

  abstract deleteEntity(entity): Observable<any>;
}
