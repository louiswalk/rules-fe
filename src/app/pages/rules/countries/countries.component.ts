import {Component} from '@angular/core';

import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import {CountriesService} from './countries.service';
import {CurrencyEditorComponent} from '../drop-down-components/currency-editor-component';
import {CountryGroupEditorComponent} from '../drop-down-components/country-group-editor-component';

@Component({
  selector: 'ngx-countries',
  styleUrls: ['./countries.component.scss'],
  templateUrl: './countries.component.html',
})
export class CountriesComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      countryCode: {
        title: 'Country Code',
        type: 'string',
        editable: false,
      },
      description: {
        title: 'Description',
        type: 'string',
      },
      internationalDialingCode: {
        title: 'International Dialing Code',
        type: 'string',
      },
      capitalCity: {
        title: 'Capital City',
        type: 'string',
      },
      syCurrency: {
        title: 'Currency',
        type: 'html',
        editor: {
          type: 'custom',
          component: CurrencyEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.syCurrency)
            return cell.syCurrency.description;
        },
      },
      syCountryGroup: {
        title: 'Country Group',
        type: 'html',
        editor: {
          type: 'custom',
          component: CountryGroupEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.syCountryGroup)
            return cell.syCountryGroup.description;
        },
      },
    },
  };

  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 5000,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;

  constructor(private countriesService: CountriesService,
              private toasterService: ToasterService) {
    this.service = countriesService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Added expense code ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not add expense code ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'Expense Types',
            body: 'Deleted expense code ' + entity.data.name + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'Expense Types',
            body: 'Could not delete expense code ' + entity.data.name,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Updated expense code ' + entity.data.name + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not update expense code ' + entity.data.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
