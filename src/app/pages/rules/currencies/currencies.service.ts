import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CurrenciesService {

  entityURL: string = 'api/currencies';

  constructor(private http: HttpClient) {
  }

  addEntity(entity): Observable<any> {
    return this.http.post<any>(this.entityURL, entity);
  }

  deleteEntity(entity): Observable<any> {
    return this.http.delete<any>(this.entityURL + '/' + entity.countryCode);
  }

  getEntities(): Observable<any> {
    return this.http.get<any>(this.entityURL);
  }

  updateEntity(entity): Observable<any> {
    return this.http.put<any>(this.entityURL + '/' + entity.countryCode, entity);
  }
}
