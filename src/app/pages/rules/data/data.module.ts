import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {VatAuthoritiesService} from '../vat-authorities/vat-authorities.service';
import {DuGroupsService} from '../du-groups/du-groups.service';
import {ExpenseAgreementsService} from '../expense-agreements/expense-agreements.service';
import {ExpenseAgreementVatRatesService} from '../expense-agreement-vat-rates/expense-agreement-vat-rates.service';
import {ExpenseTypeCodesService} from '../expense-type-codes/expense-type-codes.service';
import {ExpenseTypeCategoriesService} from '../expense-type-categories/expense-type-categories.service';
import {ExpenseTypeSubCodesService} from '../expense-type-sub-codes/expense-type-sub-codes.service';
import {ExpenseTypesService} from '../expense-types/expense-types.service';
import {ReciprocityService} from '../reciprocity/reciprocity.service';
import {SupportingDocumentsService} from '../supporting-documents/supporting-documents.service';
import {VatSchemeExpenseAgreementsService} from '../vat-scheme-expense-agreements/vat-scheme-expense-agreements.service';
import {VatSchemeExpenseTypesService} from '../vat-scheme-expense-types/vat-scheme-expense-types.service';
import {VatSchemePeriodDescriptionsService} from '../vat-scheme-period-descriptions/vat-scheme-period-descriptions.service';
import {VatSchemePeriodTypesService} from '../vat-scheme-period-types/vat-scheme-period-types.service';
import {VatSchemePeriodsService} from '../vat-scheme-periods/vat-scheme-periods.service';
import {VatSchemeSupportingDocumentsService} from '../vat-scheme-supporting-documents/vat-scheme-supporting-documents.service';
import {VatSchemeTypesService} from '../vat-scheme-types/vat-scheme-types.service';
import {VatSchemesService} from '../vat-schemes/vat-schemes.service';
import {CountriesService} from '../countries/countries.service';
import {CountryGroupsService} from '../country-groups/country-groups.service';
import {CurrenciesService} from '../currencies/currencies.service';
import {VatRuleLookupCategoriesService} from '../vat-rule-lookup-categories/vat-rule-lookup-categories.service';
import {VatRuleLookupItemsService} from '../vat-rule-lookup-items/vat-rule-lookup-items.service';
import {InvoiceStatusesService} from '../invoice-statuses/invoice-statuses.service';

const SERVICES = [
  DuGroupsService,
  ExpenseAgreementsService,
  ExpenseAgreementVatRatesService,
  ExpenseTypeCodesService,
  ExpenseTypeCategoriesService,
  ExpenseTypeSubCodesService,
  ExpenseTypesService,
  ReciprocityService,
  SupportingDocumentsService,
  VatAuthoritiesService,
  VatSchemeExpenseAgreementsService,
  VatSchemeExpenseTypesService,
  VatSchemePeriodDescriptionsService,
  VatSchemePeriodTypesService,
  VatSchemePeriodsService,
  VatSchemeSupportingDocumentsService,
  VatSchemeTypesService,
  VatSchemesService,
  CountriesService,
  CountryGroupsService,
  CurrenciesService,
  VatRuleLookupCategoriesService,
  VatRuleLookupItemsService,
  InvoiceStatusesService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
