import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {CountriesService} from '../countries/countries.service';

@Component({
  selector: 'ngx-countries-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)"  [items]="countries"
               bindLabel="description"
               bindValue="countryCode"
               [(ngModel)]="selectedCountryCode">
    </ng-select>
  `,
})
export class CountryEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public countries = [];
  public selectedCountryCode;

  constructor(private countriesService: CountriesService) {
    super();
  }

  getCountries() {
    this.countriesService.getEntities().subscribe(
      data => {
        this.countries = data;
        this.selectedCountryCode = this.cell.newValue.countryCode;
        if (this.selectedCountryCode === undefined) {
          this.selectedCountryCode = this.cell.row.data.syCountry.countryCode
        }
      },
    );
  }
  setId(value) {
    this.cell.newValue = {
      countryCode: value,
    };
  }
  ngOnInit(): void {
    this.getCountries();
  }
}
