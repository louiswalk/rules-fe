import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {CountryGroupsService} from '../country-groups/country-groups.service';

@Component({
  selector: 'ngx-country-groups-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="entities"
               bindLabel="description"
               bindValue="countryGroupId"
               [(ngModel)]="selectedEntity">
    </ng-select>
  `,
})
export class CountryGroupEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public entities = [];
  public selectedEntity;

  constructor(private service: CountryGroupsService) {
    super();
  }

  getEntities() {
    this.service.getEntities().subscribe(
      data => {
        this.entities = data;
        this.selectedEntity = this.cell.newValue.currencyGroupId;
        if (this.selectedEntity === undefined) {
          this.selectedEntity = this.cell.row.data.syCountryGroup.countryGroupId
        }
      },
    );
  }
  setId(value) {
    this.cell.newValue = {
      countryGroupId: value,
    };
  }
  ngOnInit(): void {
    this.getEntities();
  }
}
