import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {CurrenciesService} from '../currencies/currencies.service';

@Component({
  selector: 'ngx-currency-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)"  [items]="entities"
               bindLabel="description"
               bindValue="currencyCode"
               [(ngModel)]="selectedEntity">
    </ng-select>
  `,
})
export class CurrencyEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public entities = [];
  public selectedEntity;

  constructor(private service: CurrenciesService) {
    super();
  }

  getEntities() {
    this.service.getEntities().subscribe(
      data => {
        this.entities = data;
        this.selectedEntity = this.cell.newValue.currencyCode;
        if (this.selectedEntity === undefined) {
          this.selectedEntity = this.cell.row.data.syCurrency.currencyCode
        }
      },
    );
  }
  setId(value) {
    this.cell.newValue = {
      currencyCode: value,
    };
  }
  ngOnInit(): void {
    this.getEntities();
  }
}
