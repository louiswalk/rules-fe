import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {DuGroupsService} from '../du-groups/du-groups.service';

@Component({
  selector: 'ngx-du-group-submission-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)"  [items]="entities"
               bindLabel="name"
               bindValue="groupId"
               [(ngModel)]="selectedEntity">
    </ng-select>
  `,
})
export class DuGroupSubmissionEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public entities = [];
  public selectedEntity;

  constructor(private service: DuGroupsService) {
    super();
  }

  getEntities() {
    this.service.getEntities().subscribe(
      data => {
        this.entities = data;
        if (this.selectedEntity === undefined) {
          if (this.cell.row.data.submissionImageGrpId)
            this.selectedEntity = this.cell.row.data.submissionImageGrpId.groupId
        }
      },
    );
  }
  setId(value) {
    this.cell.newValue = {
      groupId: value,
    };
  }
  ngOnInit(): void {
    this.getEntities();
  }
}
