import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {ExpenseTypeCategoriesService} from '../expense-type-categories/expense-type-categories.service';

@Component({
  selector: 'ngx-expense-type-category-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)"  [items]="expenseTypeCategories"
               bindLabel="description"
               bindValue="expenseTypeCategoryId"
               [(ngModel)]="selectedExpenseTypeCategory">
    </ng-select>
  `,
})
export class ExpenseTypeCategoryEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public expenseTypeCategories = [];
  public selectedExpenseTypeCategory;

  constructor(private expenseTypeCategoriesService: ExpenseTypeCategoriesService) {
    super();
  }

  getCountries() {
    this.expenseTypeCategoriesService.getEntities().subscribe(
      data => {
        this.expenseTypeCategories = data;
        this.selectedExpenseTypeCategory = this.cell.row.data.vtExpenseTypeCategory.expenseTypeCategoryId;
      },
    );
  }
  setId(value) {
    this.cell.newValue = {
      expenseTypeCategoryId: value,
    };
  }
  ngOnInit(): void {
    this.getCountries();
  }
}
