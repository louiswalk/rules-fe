import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {ExpenseTypeCodesService} from '../expense-type-codes/expense-type-codes.service';

@Component({
  selector: 'ngx-expense-type-code-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)"  [items]="expenseTypeCodes"
               bindLabel="description"
               bindValue="codeId"
               [(ngModel)]="selectedExpenseTypeCodes">
    </ng-select>
  `,
})
export class ExpenseTypeCodeEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public expenseTypeCodes = [];
  public selectedExpenseTypeCodes;

  constructor(private expenseTypeCodesService: ExpenseTypeCodesService) {
    super();
  }

  getExpenseTypeCodes() {
    this.expenseTypeCodesService.getEntities().subscribe(
      data => {
        this.expenseTypeCodes = data;
        this.selectedExpenseTypeCodes = this.cell.row.data.vtExpenseTypeCode.codeId;
      },
    );
  }
  setId(value) {
    this.cell.newValue = {
      codeId: value,
    };
  }
  ngOnInit(): void {
    this.getExpenseTypeCodes();
  }
}
