import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {ExpenseTypesService} from '../expense-types/expense-types.service';

@Component({
  selector: 'ngx-expense-type-code-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)"  [items]="expenseTypes"
               bindLabel="name"
               bindValue="expenseTypeId"
               [(ngModel)]="selectedExpenseType">
    </ng-select>
  `,
})
export class ExpenseTypeEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public expenseTypes = [];
  public selectedExpenseType;

  constructor(private expenseTypesService: ExpenseTypesService) {
    super();
  }

  getExpenseTypes() {
    this.expenseTypesService.getEntities().subscribe(
      data => {
        this.expenseTypes = data;
        this.selectedExpenseType = this.cell.row.data.vtExpenseType.expenseTypeId;
      },
    );
  }
  setId(value) {
    this.cell.newValue = {
      expenseTypeId: value,
    };
  }
  ngOnInit(): void {
    this.getExpenseTypes();
  }
}
