import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {ExpenseTypeSubCodesService} from '../expense-type-sub-codes/expense-type-sub-codes.service';

@Component({
  selector: 'ngx-expense-type-sub-code-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="expenseTypeSubCodes"
               bindLabel="description"
               bindValue="subCodeId"
               [(ngModel)]="selectedExpenseType">
    </ng-select>
  `,
})
export class ExpenseTypeSubCodeEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public expenseTypeSubCodes = [];
  public selectedExpenseType;

  constructor(private expenseTypeSubCodesService: ExpenseTypeSubCodesService) {
    super();
  }

  getCountries() {
    this.expenseTypeSubCodesService.getEntities().subscribe(
      data => {
        this.expenseTypeSubCodes = data;
        this.selectedExpenseType = this.cell.row.data.vtExpenseTypeSubCode.subCodeId;
      },
      onFailure => {

      },
    );
  }
  setId(value) {
    this.cell.newValue = {
      subCodeId: value,
    };
  }
  ngOnInit(): void {
    this.getCountries();
  }
}
