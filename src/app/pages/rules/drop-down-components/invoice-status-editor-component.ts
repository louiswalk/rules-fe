import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {InvoiceStatusesService} from '../invoice-statuses/invoice-statuses.service';

@Component({
  selector: 'ngx-invoice-status-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="entities"
               bindLabel="name"
               bindValue="invoiceStatusCode"
               [(ngModel)]="selectedEntity">
    </ng-select>
  `,
})
export class InvoiceStatusEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public entities = [];
  public selectedEntity;

  constructor(private service: InvoiceStatusesService) {
    super();
  }

  getEntities() {
    this.service.getEntities().subscribe(
      data => {
        this.entities = data;
        this.selectedEntity = this.cell.newValue.invoiceStatusCode;
        if (this.selectedEntity === undefined) {
          this.selectedEntity = this.cell.row.data.vtInvoiceStatus.invoiceStatusCode
        }
      },
    );
  }

  setId(value) {
    this.cell.newValue = {
      invoiceStatusCode: value,
    };
  }

  ngOnInit(): void {
    this.getEntities();
  }
}
