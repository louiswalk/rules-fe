import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {VatSchemePeriodDescriptionsService} from '../vat-scheme-period-descriptions/vat-scheme-period-descriptions.service';

@Component({
  selector: 'ngx-vat-scheme-period-description-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="vatSchemePeriodDescriptions"
               bindLabel="name"
               bindValue="periodDescriptionId"
               [(ngModel)]="selectedVatSchemePeriodDescription">
    </ng-select>
  `,
})
export class PeriodDescriptionEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public vatSchemePeriodDescriptions = [];
  public selectedVatSchemePeriodDescription;

  constructor(private vatSchemePeriodDescriptionsService: VatSchemePeriodDescriptionsService) {
    super();
  }

  getExpenseTypes() {
    this.vatSchemePeriodDescriptionsService.getEntities().subscribe(
      data => {
        this.vatSchemePeriodDescriptions = data;
        if (this.cell.row.data.vtVatSchemePeriodDescription)
          this.selectedVatSchemePeriodDescription = this.cell.row.data.vtVatSchemePeriodDescription.periodDescriptionId;
      },
    );
  }

  setId(value) {
    this.cell.newValue = {
      periodDescriptionId: value,
    };
  }

  ngOnInit(): void {
    this.getExpenseTypes();
  }
}
