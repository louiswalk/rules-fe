import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {VatAuthoritiesService} from '../vat-authorities/vat-authorities.service';

@Component({
  selector: 'ngx-vat-authority-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="entities"
               bindLabel="description"
               bindValue="entityCodeVatAuthority"
               [(ngModel)]="selectedEntity">
    </ng-select>
  `,
})
export class VatAuthorityEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public entities = [];
  public selectedEntity;

  constructor(private service: VatAuthoritiesService) {
    super();
  }

  getEntities() {
    this.service.getEntities().subscribe(
      data => {
        this.entities = data;
        this.selectedEntity = this.cell.newValue.entityCodeVatAuthority;
        if (this.selectedEntity === undefined) {
          this.selectedEntity = this.cell.row.data.vtVatAuthority.entityCodeVatAuthority
        }
      },
    );
  }

  setId(value) {
    this.cell.newValue = {
      entityCodeVatAuthority: value,
    };
  }

  ngOnInit(): void {
    this.getEntities();
  }
}
