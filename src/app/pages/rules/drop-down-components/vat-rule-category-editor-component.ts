import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {VatRuleLookupCategoriesService} from '../vat-rule-lookup-categories/vat-rule-lookup-categories.service';

@Component({
  selector: 'ngx-vat-rule-category-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="entities"
               bindLabel="description"
               bindValue="categoryCode"
               [(ngModel)]="selectedEntity">
    </ng-select>
  `,
})
export class VatRuleCategoryEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public entities = [];
  public selectedEntity;

  constructor(private service: VatRuleLookupCategoriesService) {
    super();
  }

  getEntities() {
    this.service.getEntities().subscribe(
      data => {
        this.entities = data;
        this.selectedEntity = this.cell.newValue.categoryCode;
        if (this.selectedEntity === undefined) {
          this.selectedEntity = this.cell.row.data.vrRuleLookupCategory.categoryCode
        }
      },
    );
  }

  setId(value) {
    this.cell.newValue = {
      categoryCode: value,
    };
  }

  ngOnInit(): void {
    this.getEntities();
  }
}
