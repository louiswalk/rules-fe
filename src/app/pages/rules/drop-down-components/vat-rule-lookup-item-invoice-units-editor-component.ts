import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {VatRuleLookupItemsService} from '../vat-rule-lookup-items/vat-rule-lookup-items.service';

@Component({
  selector: 'ngx-rule-lookup-item-invoice-units-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="entities"
               bindLabel="description"
               bindValue="lookupItemId"
               [(ngModel)]="selectedEntity">
    </ng-select>
  `,
})
export class VatRuleLookupItemInvoiceUnitsEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public entities = [];
  public selectedEntity;

  constructor(private service: VatRuleLookupItemsService) {
    super();
  }

  getEntities() {
    this.service.getEntities().subscribe(
      data => {
        this.entities = data;
        if (this.selectedEntity === undefined) {
          if (this.cell.row.data.invoicePeriodUnits)
            this.selectedEntity = this.cell.row.data.invoicePeriodUnits.lookupItemId
        }
      },
    );
  }

  setId(value) {
    this.cell.newValue = {
      lookupItemId: value,
    };
  }

  ngOnInit(): void {
    this.getEntities();
  }
}
