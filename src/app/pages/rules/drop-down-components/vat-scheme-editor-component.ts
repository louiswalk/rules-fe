import {Component, Input, OnInit, Output} from '@angular/core';
import {VatSchemesService} from '../vat-schemes/vat-schemes.service';
import {DefaultEditor} from 'ng2-smart-table';

@Component({
  selector: 'ngx-vat-scheme-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="vatSchemes"
               bindLabel="schemeName"
               bindValue="vatSchemeId"
               [(ngModel)]="selectedVatSchemeId">
    </ng-select>
  `,
})
export class VatSchemeEditorComponent extends DefaultEditor implements OnInit {
  @Input() cell: any;
  @Output() changed: any;
  public vatSchemes = [];
  public selectedVatSchemeId;

  constructor(private vatSchemeService: VatSchemesService) {
    super();

  }

  getVatSchemes() {
    this.vatSchemeService.getEntities().subscribe(
      data => {
        this.vatSchemes = data;
        this.selectedVatSchemeId = this.cell.newValue.vatSchemeId;
        if (this.selectedVatSchemeId === undefined) {
          if (this.cell.row.data.vtVatScheme)
            this.selectedVatSchemeId = this.cell.row.data.vtVatScheme.vatSchemeId;
        }
      },
    );
  }

  setId(value) {
    this.cell.newValue = {
      vatSchemeId: value,
    };
  }

  ngOnInit(): void {
    this.getVatSchemes();
  }
}
