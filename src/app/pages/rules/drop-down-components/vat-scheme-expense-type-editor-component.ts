import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {VatSchemeExpenseTypesService} from '../vat-scheme-expense-types/vat-scheme-expense-types.service';

@Component({
  selector: 'ngx-vat-scheme-expense-type-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [(ngModel)]="selectedVatschemeExpenseType">
      <ng-option *ngFor="let vatSchemeExpenseType of vatSchemeExpenseTypes"
                 [value]="vatSchemeExpenseType.vatSchemeExpenseTypeId">
        {{vatSchemeExpenseType.vtVatScheme.schemeName + ' : ' + (vatSchemeExpenseType.vtExpenseType.code === '' ?
        vatSchemeExpenseType.vtExpenseType.vtExpenseTypeSubCode.vtExpenseTypeCode.code + ' - ' +
        vatSchemeExpenseType.vtExpenseType.vtExpenseTypeSubCode.description :
        vatSchemeExpenseType.vtExpenseType.code + ' - ' +
        vatSchemeExpenseType.vtExpenseType.vtExpenseTypeSubCode.description)}}
      </ng-option>
      <ng-option [value]="'custom'">Custom</ng-option>
    </ng-select>
  `,
})
export class VatSchemeExpenseTypeEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public vatSchemeExpenseTypes = [];
  public selectedVatschemeExpenseType;
  public items: Array<any> = [];

  constructor(private vatSchemeExpenseTypesService: VatSchemeExpenseTypesService) {
    super();
  }

  getExpenseTypes() {
    this.vatSchemeExpenseTypesService.getEntities().subscribe(
      data => {
        this.vatSchemeExpenseTypes = data;
        this.selectedVatschemeExpenseType = this.cell.row.data.vtVatSchemeExpenseType.vatSchemeExpenseTypeId;
      },
    );
  }

  setId(value) {
    this.cell.newValue = {
      vatSchemeExpenseTypeId: value,
    };
  }

  ngOnInit(): void {
    this.getExpenseTypes();
  }
}
