import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {VatSchemePeriodTypesService} from '../vat-scheme-period-types/vat-scheme-period-types.service';

@Component({
  selector: 'ngx-vat-scheme-period-type-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="vatSchemePeriodTypes"
               bindLabel="name"
               bindValue="vsPeriodTypeId"
               [(ngModel)]="selectedVatSchemePeriodType">
    </ng-select>
  `,
})
export class VatSchemePeriodTypeEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public vatSchemePeriodTypes = [];
  public selectedVatSchemePeriodType;

  constructor(private vatSchemePeriodTypesService: VatSchemePeriodTypesService) {
    super();
  }

  getExpenseTypes() {
    this.vatSchemePeriodTypesService.getEntities().subscribe(
      data => {
        this.vatSchemePeriodTypes = data;
        this.selectedVatSchemePeriodType = this.cell.row.data.vtVatSchemePeriodType.vsPeriodTypeId;
      },
    );
  }


  setId(value) {
    this.cell.newValue = {
      vsPeriodTypeId: value,
    };
  }

  ngOnInit(): void {
    this.getExpenseTypes();
  }
}
