import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {SupportingDocumentsService} from '../supporting-documents/supporting-documents.service';

@Component({
  selector: 'ngx-vat-scheme-supporting-document-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="supportingDocuments"
               bindLabel="description"
               bindValue="supportingDocumentId"
               [(ngModel)]="selectedSupportingDocument">
    </ng-select>
  `,
})
export class VatSchemeSupportingDocumentEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public supportingDocuments = [];
  public selectedSupportingDocument;

  constructor(private supportingDocumentsService: SupportingDocumentsService) {
    super();
  }

  getExpenseTypes() {
    this.supportingDocumentsService.getEntities().subscribe(
      data => {
        this.supportingDocuments = data;
        if (this.cell.row.data.vtSupportingDocument)
          this.selectedSupportingDocument = this.cell.row.data.vtSupportingDocument.supportingDocumentId;
      },
    );
  }

  setId(value) {
    this.cell.newValue = {
      supportingDocumentId: value,
    };
  }

  ngOnInit(): void {
    this.getExpenseTypes();
  }
}
