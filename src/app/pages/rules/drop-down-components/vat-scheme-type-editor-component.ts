import {Component, Input, OnInit} from '@angular/core';
import {DefaultEditor} from 'ng2-smart-table';
import {VatSchemeTypesService} from '../vat-scheme-types/vat-scheme-types.service';

@Component({
  selector: 'ngx-vat-scheme-types-drop-down',
  template: `
    <ng-select (ngModelChange)="setId($event)" [items]="entities"
               bindLabel="description"
               bindValue="vatSchemeTypeId"
               [(ngModel)]="selectedEntity">
    </ng-select>
  `,
})
export class VatSchemeTypeEditorComponent extends DefaultEditor implements OnInit {

  @Input() cell: any;
  public entities = [];
  public selectedEntity;

  constructor(private service: VatSchemeTypesService) {
    super();
  }

  getEntities() {
    this.service.getEntities().subscribe(
      data => {
        this.entities = data;
        this.selectedEntity = this.cell.newValue.vatSchemeTypeId;
        if (this.selectedEntity === undefined) {
          this.selectedEntity = this.cell.row.data.vtVatSchemeType.vatSchemeTypeId
        }
      },
    );
  }


  setId(value) {
    this.cell.newValue = {
      vatSchemeTypeId: value,
    };
  }

  ngOnInit(): void {
    this.getEntities();
  }
}
