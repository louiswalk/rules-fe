import {Component} from '@angular/core';

import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import {ExpenseAgreementVatRatesService} from './expense-agreement-vat-rates.service';
import {CrudComponent} from '../CrudComponent';
import {VatSchemeExpenseTypeEditorComponent} from '../drop-down-components/vat-scheme-expense-type-editor-component';

@Component({
  selector: 'ngx-expense-agreement-vat-rates',
  styleUrls: ['./expense-agreement-vat-rates.component.scss'],
  templateUrl: './expense-agreement-vat-rates.component.html',
})
export class ExpenseAgreementVatRatesComponent implements CrudComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      vtVatSchemeExpenseType: {
        title: 'Vat Scheme Expense Type ID',
        type: 'html',

        editor: {
          type: 'custom',
          component: VatSchemeExpenseTypeEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtVatSchemeExpenseType.vtVatScheme.schemeName + ' : ' + (cell.vtVatSchemeExpenseType.vtExpenseType.code === '' ?
            cell.vtVatSchemeExpenseType.vtExpenseType.vtExpenseTypeSubCode.vtExpenseTypeCode.code + ' - ' +
            cell.vtVatSchemeExpenseType.vtExpenseType.vtExpenseTypeSubCode.description :
            cell.vtVatSchemeExpenseType.vtExpenseType.code + ' - ' +
            cell.vtVatSchemeExpenseType.vtExpenseType.vtExpenseTypeSubCode.description);
        },
      },
      rate: {
        title: 'Rate',
        type: 'number',
        valuePrepareFunction: (date, cell) => {
          return cell.rate * 100 + '%';
        },
      },
      fromDate: {
        title: 'To Date',
        type: 'date',
      },
      toDate: {
        title: 'To Date',
        type: 'date',
      },
      regionalTaxRate: {
        title: 'Regional Tax Rate',
        type: 'string',
      },
    },
  };

  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 5000,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;

  constructor(private expenseAgreementVatRatesService: ExpenseAgreementVatRatesService,
              private toasterService: ToasterService) {
    this.service = expenseAgreementVatRatesService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Added expense code ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not add expense code ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'Expense Types',
            body: 'Deleted expense code ' + entity.data.name + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'Expense Types',
            body: 'Could not delete expense code ' + entity.data.name,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Updated expense code ' + entity.data.name + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not update expense code ' + entity.data.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
