import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ExpenseAgreementsService {

  private vatAuthoritiesUrl = 'api/vat-authorities';

  constructor(private http: HttpClient) {
  }

  getVatAuthorities(): Observable<any> {
    return this.http.get<any>(this.vatAuthoritiesUrl);
  }

  updateVatAuthority(newData): Observable<any> {
    return this.http.put<any>(this.vatAuthoritiesUrl + '/' + newData.entityCodeVatAuthority, newData);
  }

  createVatAuthority(newData): Observable<any> {
    return this.http.post<any>(this.vatAuthoritiesUrl, newData);
  }

  deleteVatAuthority(data): Observable<any> {
    return this.http.delete<any>(this.vatAuthoritiesUrl + '/' + data.entityCodeVatAuthority);
  }
}
