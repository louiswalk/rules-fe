import {Component} from '@angular/core';

import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import {ExpenseTypeSubCodesService} from './expense-type-sub-codes.service';
import {ExpenseTypeCodeEditorComponent} from '../drop-down-components/expense-type-code-editor-component';

@Component({
  selector: 'ngx-expense-type-sub-codes',
  styleUrls: ['./expense-type-sub-codes.component.scss'],
  templateUrl: './expense-type-sub-codes.component.html',
})
export class ExpenseTypeSubCodesComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      vtExpenseTypeCode: {
        title: 'Expense Code',
        type: 'html',

        editor: {
          type: 'custom',
          component: ExpenseTypeCodeEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtExpenseTypeCode.code + ' - ' + cell.vtExpenseTypeCode.description;
        },
      },
      code: {
        title: 'Sub Code',
        type: 'string',
      },
      description: {
        title: 'Description',
        type: 'string',
      },
    },
  };

  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 5000,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();

  service: any;

  constructor(private expenseTypeSubCodesService: ExpenseTypeSubCodesService,
              private toasterService: ToasterService) {
    this.service = expenseTypeSubCodesService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Sub Codes',
          body: 'Added Expense Sub Code ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Sub Codes',
          body: 'Could not add Expense Sub Code ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'Expense Sub Codes',
            body: 'Deleted Expense Sub Code ' + entity.data.description + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'Expense Sub Codes',
            body: 'Could not delete Expense Sub Code ' + entity.data.description,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'Expense Sub Codes',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'Expense Sub Codes',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Sub Codes',
          body: 'Updated Expense Sub Code ' + entity.data.description + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Sub Codes',
          body: 'Could not update Expense Sub Code ' + entity.data.description,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
