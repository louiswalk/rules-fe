import {Component} from '@angular/core';

import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import {ExpenseTypesService} from './expense-types.service';
import {CrudComponent} from '../CrudComponent';
import {ExpenseTypeSubCodeEditorComponent} from '../drop-down-components/expense-type-sub-code-editor-component';
import {ExpenseTypeCategoryEditorComponent} from '../drop-down-components/expense-type-category-editor-component';

@Component({
  selector: 'ngx-expense-types',
  styleUrls: ['./expense-types.component.scss'],
  templateUrl: './expense-types.component.html',
})
export class ExpenseTypesComponent implements CrudComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Name',
        type: 'string',
      },
      code: {
        title: 'Code',
        type: 'string',
      },
      subCodeId: {
        title: 'Sub Code',
        type: 'html',

        editor: {
          type: 'custom',
          component: ExpenseTypeSubCodeEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtExpenseTypeSubCode.code + ' - ' + cell.vtExpenseTypeSubCode.description;
        },
      },
      mandatoryComment: {
        title: 'Mandatory Comment',
        valuePrepareFunction: (date, cell) => {
          if (cell.mandatoryComment === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      vtExpenseTypeCategory: {
        title: 'Expense Type Category ID',
        type: 'html',
        editor: {
          type: 'custom',
          component: ExpenseTypeCategoryEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtExpenseTypeCategory.description;
        },
      },
    },
  };

  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 7500,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;

  constructor(private expenseTypesService: ExpenseTypesService,
              private toasterService: ToasterService) {
    this.service = expenseTypesService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Added expense code ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not add expense code ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'Expense Types',
            body: 'Deleted expense code ' + entity.data.name + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'Expense Types',
            body: 'Could not delete expense code ' + entity.data.name,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Updated expense code ' + entity.data.name + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not update expense code ' + entity.data.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
