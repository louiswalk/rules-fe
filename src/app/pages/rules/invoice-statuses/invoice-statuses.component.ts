import {Component} from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {InvoiceStatusesService} from './invoice-statuses.service';


@Component({
  selector: 'ngx-invoice-statuses',
  styleUrls: ['./invoice-statuses.component.scss'],
  templateUrl: './invoice-statuses.component.html',
})
export class InvoiceStatusesComponent {

  vatSchemes = [];
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      invoiceStatusCode: {
        title: 'Invoice Status Code',
        type: 'string',
      },
      name: {
        title: 'Name',
        type: 'string',
      },
      invoiceCapture: {
        title: 'Invoice Capture',
        valuePrepareFunction: (date, cell) => {
          if (cell.invoiceCapture === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
    },
  };


  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 5000,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;
  number = 0;

  constructor(private vatSchemeExpenseTypesService: InvoiceStatusesService,
              private toasterService: ToasterService) {
    this.service = vatSchemeExpenseTypesService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'VAT Scheme Expense Types',
          body: 'Added vat scheme expense type ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'VAT Scheme Expense Types',
          body: 'Could not add vat scheme expense type ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'VAT Scheme Expense Types',
            body: 'Deleted vat scheme expense type ' + entity.data.description + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'VAT Scheme Expense Types',
            body: 'Could not delete vat scheme expense type ' + entity.data.description,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'VAT Scheme Expense Types',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'VAT Scheme Expense Types',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'VAT Scheme Expense Types',
          body: 'Updated vat scheme expense type ' + entity.data.description + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'VAT Scheme Expense Types',
          body: 'Could not update vat scheme expense type ' + entity.data.description,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
