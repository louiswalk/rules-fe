import {Component} from '@angular/core';

import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {ReciprocityService} from './reciprocity.service';
import {CountryEditorComponent} from '../drop-down-components/country-editor-component';

@Component({
  selector: 'ngx-reciprocity',
  styleUrls: ['./reciprocity.component.scss'],
  templateUrl: './reciprocity.component.html',
})
export class ReciprocityComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      clientCountry: {
        title: 'Client Country',
        type: 'html',

        editor: {
          type: 'custom',
          component: CountryEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.clientCountry.description
        },
      },
      vaCountry: {
        title: 'Vat Authority Country',
        type: 'html',
        editor: {
          type: 'custom',
          component: CountryEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vaCountry.description
        },
      },
    },
  };


  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 5000,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;

  constructor(private reciprocityService: ReciprocityService,
              private toasterService: ToasterService) {
    this.service = reciprocityService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Reciprocity',
          body: 'Added reciprocity for country  ' + entity.newData.clientCountry + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Reciprocity',
          body: 'Could not add reciprocity for country  ' + entity.newData.clientCountry,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'Reciprocity',
            body: 'Deleted reciprocity for country  ' + entity.data.clientCountry + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'Reciprocity',
            body: 'Could not delete reciprocity for country  ' + entity.data.clientCountry,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'Reciprocity',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'Reciprocity',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Reciprocity',
          body: 'Updated reciprocity for country  ' + entity.data.clientCountry + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Reciprocity',
          body: 'Could not update reciprocity for country  ' + entity.data.clientCountry,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
