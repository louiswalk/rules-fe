import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RulesComponent} from './rules.component';
import {VatAuthoritiesComponent} from './vat-authorities/vat-authorities.component';
import {ExpenseAgreementsComponent} from './expense-agreements/expense-agreements.component';
import {VatSchemesComponent} from './vat-schemes/vat-schemes.component';
import {ReciprocityComponent} from './reciprocity/reciprocity.component';
import {DuGroupsComponent} from './du-groups/du-groups.component';
import {ExpenseAgreementVatRatesComponent} from './expense-agreement-vat-rates/expense-agreement-vat-rates.component';
import {ExpenseTypeCategoriesComponent} from './expense-type-categories/expense-type-categories.component';
import {ExpenseTypeCodesComponent} from './expense-type-codes/expense-type-codes.component';
import {ExpenseTypeSubCodesComponent} from './expense-type-sub-codes/expense-type-sub-codes.component';
import {ExpenseTypesComponent} from './expense-types/expense-types.component';
import {SupportingDocumentsComponent} from './supporting-documents/supporting-documents.component';
import {VatSchemeExpenseAgreementsComponent} from './vat-scheme-expense-agreements/vat-scheme-expense-agreements.component';
import {VatSchemeExpenseTypesComponent} from './vat-scheme-expense-types/vat-scheme-expense-types.component';
import {VatSchemePeriodDescriptionsComponent} from './vat-scheme-period-descriptions/vat-scheme-period-descriptions.component';
import {VatSchemePeriodTypesComponent} from './vat-scheme-period-types/vat-scheme-period-types.component';
import {VatSchemePeriodsComponent} from './vat-scheme-periods/vat-scheme-periods.component';
import {VatSchemeSupportingDocumentsComponent} from './vat-scheme-supporting-documents/vat-scheme-supporting-documents.component';
import {VatSchemeTypesComponent} from './vat-scheme-types/vat-scheme-types.component';
import {CurrenciesComponent} from './currencies/currencies.component';
import {CountryGroupsComponent} from './country-groups/country-groups.component';
import {CountriesComponent} from './countries/countries.component';
import {InvoiceStatusesComponent} from './invoice-statuses/invoice-statuses.component';
import {VatRuleLookupCategoriesComponent} from './vat-rule-lookup-categories/vat-rule-lookup-categories.component';
import {VatRuleLookupItemsComponent} from './vat-rule-lookup-items/vat-rule-lookup-items.component';

const routes: Routes = [{
  path: '',
  component: RulesComponent,
  children: [{
    path: 'du-groups',
    component: DuGroupsComponent,
  },
    {
      path: 'countries',
      component: CountriesComponent,
    },
    {
      path: 'country-groups',
      component: CountryGroupsComponent,
    },
    {
      path: 'currencies',
      component: CurrenciesComponent,
    },
    {
      path: 'expense-agreements',
      component: ExpenseAgreementsComponent,
    },
    {
      path: 'expense-agreement-vat-rates',
      component: ExpenseAgreementVatRatesComponent,
    },
    {
      path: 'expense-type-categories',
      component: ExpenseTypeCategoriesComponent,
    },
    {
      path: 'expense-type-codes',
      component: ExpenseTypeCodesComponent,
    },
    {
      path: 'expense-type-sub-codes',
      component: ExpenseTypeSubCodesComponent,
    },
    {
      path: 'expense-types',
      component: ExpenseTypesComponent,
    },
    {
      path: 'reciprocity',
      component: ReciprocityComponent,
    },
    {
      path: 'supporting-documents',
      component: SupportingDocumentsComponent,
    },
    {
      path: 'vat-rule-lookup-items',
      component: VatRuleLookupItemsComponent,
    },
    {
      path: 'vat-rule-lookup-categories',
      component: VatRuleLookupCategoriesComponent,
    },
    {
      path: 'invoice-statuses',
      component: InvoiceStatusesComponent,
    },
    {
      path: 'vat-scheme-expense-agreements',
      component: VatSchemeExpenseAgreementsComponent,
    },
    {
      path: 'vat-scheme-expense-types',
      component: VatSchemeExpenseTypesComponent,
    },
    {
      path: 'vat-scheme-period-descriptions',
      component: VatSchemePeriodDescriptionsComponent,
    },
    {
      path: 'vat-scheme-period-types',
      component: VatSchemePeriodTypesComponent,
    },
    {
      path: 'vat-scheme-periods',
      component: VatSchemePeriodsComponent,
    },
    {
      path: 'vat-scheme-types',
      component: VatSchemeTypesComponent,
    },
    {
      path: 'vat-scheme-supporting-documents',
      component: VatSchemeSupportingDocumentsComponent,
    },
    {
      path: 'vat-scheme-periods',
      component: VatSchemePeriodsComponent,
    },
    {
      path: 'vat-schemes',
      component: VatSchemesComponent,
    },
    {
      path: 'vat-authorities',
      component: VatAuthoritiesComponent,
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RulesRoutingModule {
}
