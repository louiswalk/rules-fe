import {Component} from '@angular/core';

@Component({
  selector: 'ngx-rules',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class RulesComponent {
}
