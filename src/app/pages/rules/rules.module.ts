import {NgModule} from '@angular/core';

import {ThemeModule} from '../../@theme/theme.module';
import {RulesComponent} from './rules.component';
import {RulesRoutingModule} from './rules-routing.module';
import {VatAuthoritiesComponent} from './vat-authorities/vat-authorities.component';
import {VatSchemesComponent} from './vat-schemes/vat-schemes.component';
import {ExpenseAgreementsComponent} from './expense-agreements/expense-agreements.component';
import {ReciprocityComponent} from './reciprocity/reciprocity.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {DataModule} from './data/data.module';
import {ToasterModule} from 'angular2-toaster';
import {DuGroupsComponent} from './du-groups/du-groups.component';
import {ExpenseAgreementVatRatesComponent} from './expense-agreement-vat-rates/expense-agreement-vat-rates.component';
import {ExpenseTypesComponent} from './expense-types/expense-types.component';
import {ExpenseTypeCategoriesComponent} from './expense-type-categories/expense-type-categories.component';
import {ExpenseTypeCodesComponent} from './expense-type-codes/expense-type-codes.component';
import {ExpenseTypeSubCodesComponent} from './expense-type-sub-codes/expense-type-sub-codes.component';
import {SupportingDocumentsComponent} from './supporting-documents/supporting-documents.component';
import {VatSchemeExpenseAgreementsComponent} from './vat-scheme-expense-agreements/vat-scheme-expense-agreements.component';
import {VatSchemeExpenseTypesComponent} from './vat-scheme-expense-types/vat-scheme-expense-types.component';
import {VatSchemePeriodDescriptionsComponent} from './vat-scheme-period-descriptions/vat-scheme-period-descriptions.component';
import {VatSchemePeriodTypesComponent} from './vat-scheme-period-types/vat-scheme-period-types.component';
import {VatSchemePeriodsComponent} from './vat-scheme-periods/vat-scheme-periods.component';
import {VatSchemeSupportingDocumentsComponent} from './vat-scheme-supporting-documents/vat-scheme-supporting-documents.component';
import {VatSchemeTypesComponent} from './vat-scheme-types/vat-scheme-types.component';
import {VatSchemeEditorComponent} from './drop-down-components/vat-scheme-editor-component';
import {NgSelectModule} from '@ng-select/ng-select';
import {CountriesComponent} from './countries/countries.component';
import {CountryEditorComponent} from './drop-down-components/country-editor-component';
import {ExpenseTypeSubCodeEditorComponent} from './drop-down-components/expense-type-sub-code-editor-component';
import {ExpenseTypeCategoryEditorComponent} from './drop-down-components/expense-type-category-editor-component';
import {ExpenseTypeCodeEditorComponent} from './drop-down-components/expense-type-code-editor-component';
import {ExpenseTypeEditorComponent} from './drop-down-components/expense-type-editor-component';
import {PeriodDescriptionEditorComponent} from './drop-down-components/period-description-editor-component';
import {VatSchemePeriodTypeEditorComponent} from './drop-down-components/vat-scheme-period-type-editor-component';
import {VatSchemeSupportingDocumentEditorComponent} from './drop-down-components/vat-scheme-supporting-document-editor-component';
import {VatSchemeExpenseTypeEditorComponent} from './drop-down-components/vat-scheme-expense-type-editor-component';
import {CountryGroupsComponent} from './country-groups/country-groups.component';
import {CurrenciesComponent} from './currencies/currencies.component';
import {CountryGroupEditorComponent} from './drop-down-components/country-group-editor-component';
import {CurrencyEditorComponent} from './drop-down-components/currency-editor-component';
import {VatAuthorityEditorComponent} from './drop-down-components/vat-authority-editor-component';
import {VatRuleCategoryEditorComponent} from './drop-down-components/vat-rule-category-editor-component';
import {VatRuleLookupCategoriesComponent} from './vat-rule-lookup-categories/vat-rule-lookup-categories.component';
import {VatRuleLookupItemsComponent} from './vat-rule-lookup-items/vat-rule-lookup-items.component';
import {InvoiceStatusesComponent} from './invoice-statuses/invoice-statuses.component';
import {VatSchemeTypeEditorComponent} from './drop-down-components/vat-scheme-type-editor-component';
import {VatRuleLookupItemEditorComponent} from './drop-down-components/vat-rule-lookup-item-editor-component';
import {InvoiceStatusEditorComponent} from './drop-down-components/invoice-status-editor-component';
import {DuGroupEditorComponent} from './drop-down-components/du-group-editor-component';
import {DuGroupSubmissionEditorComponent} from './drop-down-components/du-group-submission-editor-component';
import {VatRuleLookupItemInvoiceUnitsEditorComponent} from './drop-down-components/vat-rule-lookup-item-invoice-units-editor-component';
import {VatRuleLookupItemDeadlineUnitEditorComponent} from './drop-down-components/vat-rule-lookup-item-deadline-unit-editor-component';
import {VatRuleLookupItemInvoiceDateCompareEditorComponent,} from './drop-down-components/vat-rule-lookup-item-invoice-date-compare-editor-component';

const components = [
  RulesComponent,
  DuGroupsComponent,
  ExpenseAgreementsComponent,
  ExpenseAgreementVatRatesComponent,
  ExpenseAgreementsComponent,
  ExpenseTypesComponent,
  ExpenseTypeCategoriesComponent,
  ExpenseTypeCodesComponent,
  ExpenseTypeSubCodesComponent,
  ReciprocityComponent,
  SupportingDocumentsComponent,
  VatAuthoritiesComponent,
  VatSchemeExpenseAgreementsComponent,
  VatSchemeExpenseTypesComponent,
  VatSchemePeriodDescriptionsComponent,
  VatSchemePeriodTypesComponent,
  VatSchemePeriodsComponent,
  VatSchemeSupportingDocumentsComponent,
  VatSchemeTypesComponent,
  VatSchemesComponent,
  VatSchemeEditorComponent,
  CountryEditorComponent,
  ExpenseTypeSubCodeEditorComponent,
  ExpenseTypeCategoryEditorComponent,
  ExpenseTypeCodeEditorComponent,
  ExpenseTypeEditorComponent,
  PeriodDescriptionEditorComponent,
  VatSchemePeriodTypeEditorComponent,
  VatSchemeSupportingDocumentEditorComponent,
  VatSchemeExpenseTypeEditorComponent,
  VatRuleCategoryEditorComponent,
  VatAuthorityEditorComponent,
  CurrencyEditorComponent,
  CountryGroupEditorComponent,
  CountriesComponent,
  CountryGroupsComponent,
  CurrenciesComponent,
  VatRuleLookupCategoriesComponent,
  VatRuleLookupItemsComponent,
  InvoiceStatusesComponent,
  VatSchemeTypeEditorComponent,
  VatRuleLookupItemEditorComponent,
  InvoiceStatusEditorComponent,
  DuGroupEditorComponent,
  DuGroupSubmissionEditorComponent,
  VatRuleLookupItemInvoiceDateCompareEditorComponent,
  VatRuleLookupItemDeadlineUnitEditorComponent,
  VatRuleLookupItemInvoiceUnitsEditorComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    RulesRoutingModule,
    Ng2SmartTableModule,
    DataModule,
    ToasterModule.forRoot(),
    NgSelectModule,
  ],
  declarations: [
    ...components,
  ],
  entryComponents: [
    VatSchemeEditorComponent,
    CountryEditorComponent,
    ExpenseTypeSubCodeEditorComponent,
    ExpenseTypeCategoryEditorComponent,
    ExpenseTypeCodeEditorComponent,
    ExpenseTypeEditorComponent,
    PeriodDescriptionEditorComponent,
    VatSchemePeriodTypeEditorComponent,
    VatSchemeSupportingDocumentEditorComponent,
    CurrencyEditorComponent,
    CountryGroupEditorComponent,
    VatSchemeExpenseTypeEditorComponent,
    VatAuthorityEditorComponent,
    VatRuleCategoryEditorComponent,
    VatSchemeTypeEditorComponent,
    VatRuleLookupItemEditorComponent,
    InvoiceStatusEditorComponent,
    DuGroupEditorComponent,
    DuGroupSubmissionEditorComponent,
    VatRuleLookupItemInvoiceDateCompareEditorComponent,
    VatRuleLookupItemDeadlineUnitEditorComponent,
    VatRuleLookupItemInvoiceUnitsEditorComponent,
  ],
})
export class RulesModule {
}
