import {Component} from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {VatSchemeExpenseAgreementsService} from './vat-scheme-expense-agreements.service';
import {DatePipe} from '@angular/common';
import {VatSchemeExpenseTypeEditorComponent} from '../drop-down-components/vat-scheme-expense-type-editor-component';
import {CountryEditorComponent} from '../drop-down-components/country-editor-component';


@Component({
  selector: 'ngx-vat-scheme-expense-agreements',
  styleUrls: ['./vat-scheme-expense-agreements.component.scss'],
  templateUrl: './vat-scheme-expense-agreements.component.html',
})
export class VatSchemeExpenseAgreementsComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      vtVatSchemeExpenseType: {
        title: 'Vat Scheme Expense Type ID',
        type: 'html',

        editor: {
          type: 'custom',
          component: VatSchemeExpenseTypeEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtVatSchemeExpenseType.vtVatScheme.schemeName + ' : ' + (cell.vtVatSchemeExpenseType.vtExpenseType.code === '' ?
            cell.vtVatSchemeExpenseType.vtExpenseType.vtExpenseTypeSubCode.vtExpenseTypeCode.code + ' - ' +
            cell.vtVatSchemeExpenseType.vtExpenseType.vtExpenseTypeSubCode.description :
            cell.vtVatSchemeExpenseType.vtExpenseType.code + ' - ' +
            cell.vtVatSchemeExpenseType.vtExpenseType.vtExpenseTypeSubCode.description);
        },
      },
      entityCode: {
        title: 'Entity Code',
        type: 'string',
      },
      countryCode: {
        title: 'Country Code',
        type: 'html',

        editor: {
          type: 'custom',
          component: CountryEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.countryCode;
        },
      },
      refundPercent: {
        title: 'Refund Percentage',
        type: 'number',
      },
      overRideInvoiceExpRule: {
        title: 'Override Invoice expiry',
        valuePrepareFunction: (date, cell) => {
          if (cell.overRideInvoiceExpRule === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      validFromDate: {
        title: 'Valid From',
        type: 'date',
        valuePrepareFunction: (date, cell) => {
          return this.datePipe.transform(cell.validFromDate, 'dd-MM-yyyy')
        },
      },
      validToDate: {
        title: 'Valid To',
        type: 'date',
        valuePrepareFunction: (date, cell) => {
          return this.datePipe.transform(cell.validToDate, 'dd-MM-yyyy')
        },
      },
      warningComment: {
        title: 'Warning Comment',
        type: 'string',
      },
      minValueForScan: {
        title: 'Minimum Value For Scan',
        type: 'number',
      },
    },
  };

  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 5000,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;

  constructor(private vatSchemeExpenseAgreementsService: VatSchemeExpenseAgreementsService,
              private toasterService: ToasterService, private datePipe: DatePipe) {
    this.service = vatSchemeExpenseAgreementsService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'VAT Scheme Expense Agreements',
          body: 'Added vat scheme expense agreements ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'VAT Scheme Expense Agreements',
          body: 'Could not add vat scheme expense agreements ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'VAT Scheme Expense Agreements',
            body: 'Deleted vat scheme expense agreements ' + entity.data.description + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'VAT Scheme Expense Agreements',
            body: 'Could not delete vat scheme expense agreements ' + entity.data.description,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'VAT Scheme Expense Agreements',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'VAT Scheme Expense Agreements',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'VAT Scheme Expense Agreements',
          body: 'Updated vat scheme expense agreements ' + entity.data.description + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'VAT Scheme Expense Agreements',
          body: 'Could not update vat scheme expense agreements ' + entity.data.description,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
