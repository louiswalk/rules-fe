import {Component} from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {VatSchemeExpenseTypesService} from './vat-scheme-expense-types.service';
import {DatePipe} from '@angular/common';
import {VatSchemesService} from '../vat-schemes/vat-schemes.service';
import {VatSchemeEditorComponent} from '../drop-down-components/vat-scheme-editor-component';
import {ExpenseTypeEditorComponent} from '../drop-down-components/expense-type-editor-component';


@Component({
  selector: 'ngx-vat-scheme-expense-types',
  styleUrls: ['./vat-scheme-expense-types.component.scss'],
  templateUrl: './vat-scheme-expense-types.component.html',
})
export class VatSchemeExpenseTypesComponent {

  vatSchemes = [];
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      vtVatSchemeExpenseType: {
        title: 'Expense Type',
        type: 'html',

        editor: {
          type: 'custom',
          component: ExpenseTypeEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtExpenseType.code === '' ?
            cell.vtExpenseType.vtExpenseTypeSubCode.vtExpenseTypeCode.code + ' - ' + cell.vtExpenseType.vtExpenseTypeSubCode.description :
            cell.vtExpenseType.code + ' - ' + cell.vtExpenseType.vtExpenseTypeSubCode.description;
        },
      },
      vtVatScheme: {
        title: 'Vat Scheme',
        type: 'html',

        editor: {
          type: 'custom',
          component: VatSchemeEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtVatScheme.schemeName
        },
      },
      active: {
        title: 'Active',
        valuePrepareFunction: (date, cell) => {
          if (cell.active === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      zeroRated: {
        title: 'Zero Rated',

        valuePrepareFunction: (date, cell) => {
          if (cell.zeroRated === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
    },
  };


  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 5000,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;
  number = 0;

  constructor(private vatSchemeExpenseTypesService: VatSchemeExpenseTypesService, private vatSchemeService: VatSchemesService,
              private toasterService: ToasterService, private datePipe: DatePipe) {
    this.service = vatSchemeExpenseTypesService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'VAT Scheme Expense Types',
          body: 'Added vat scheme expense type ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'VAT Scheme Expense Types',
          body: 'Could not add vat scheme expense type ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'VAT Scheme Expense Types',
            body: 'Deleted vat scheme expense type ' + entity.data.description + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'VAT Scheme Expense Types',
            body: 'Could not delete vat scheme expense type ' + entity.data.description,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'VAT Scheme Expense Types',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'VAT Scheme Expense Types',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'VAT Scheme Expense Types',
          body: 'Updated vat scheme expense type ' + entity.data.description + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'VAT Scheme Expense Types',
          body: 'Could not update vat scheme expense type ' + entity.data.description,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
