import {Component} from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {VatSchemePeriodsService} from './vat-scheme-periods.service';
import {PeriodDescriptionEditorComponent} from "../drop-down-components/period-description-editor-component";
import {VatSchemePeriodTypeEditorComponent} from "../drop-down-components/vat-scheme-period-type-editor-component";


@Component({
  selector: 'ngx-vat-scheme-periods',
  styleUrls: ['./vat-scheme-periods.component.scss'],
  templateUrl: './vat-scheme-periods.component.html',
})
export class VatSchemePeriodsComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      vtVatSchemePeriodDescription: {
        title: 'Period Description',
        type: 'html',

        editor: {
          type: 'custom',
          component: PeriodDescriptionEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtVatSchemePeriodDescription.name + ' - ' + cell.vtVatSchemePeriodDescription.yearEndDate;
        },
      },

      vtVatSchemePeriodType: {
        title: 'Vat Scheme Period Type',
        type: 'html',

        editor: {
          type: 'custom',
          component: VatSchemePeriodTypeEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtVatSchemePeriodType.name + ' - ' + cell.vtVatSchemePeriodType.description;
        },
      },
      startMonth: {
        title: 'Start Month',
        type: 'number',
      },
      endMonth: {
        title: 'End Month',
        type: 'number',
      },
      description: {
        title: 'Description',
        type: 'string',
      },
      incrementYear: {
        title: 'Increment year',
        type: 'number',
      },
      claimYearStartIncrement: {
        title: 'Claim Year Start Increment',
        type: 'number',
      },
    },
  };

  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 7500,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;

  constructor(private vatSchemePeriodsService: VatSchemePeriodsService,
              private toasterService: ToasterService) {
    this.service = vatSchemePeriodsService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Added expense code ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not add expense code ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'Expense Types',
            body: 'Deleted expense code ' + entity.data.name + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'Expense Types',
            body: 'Could not delete expense code ' + entity.data.name,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Updated expense code ' + entity.data.name + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not update expense code ' + entity.data.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
