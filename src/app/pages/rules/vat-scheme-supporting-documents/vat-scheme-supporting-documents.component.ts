import {Component} from '@angular/core';

import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import {VatSchemeSupportingDocumentsService} from './vat-scheme-supporting-documents.service';
import {PeriodDescriptionEditorComponent} from '../drop-down-components/period-description-editor-component';
import {VatSchemeEditorComponent} from '../drop-down-components/vat-scheme-editor-component';
import {VatSchemeSupportingDocumentEditorComponent} from '../drop-down-components/vat-scheme-supporting-document-editor-component';

@Component({
  selector: 'ngx-vat-scheme-supporting-documents',
  styleUrls: ['./vat-scheme-supporting-documents.component.scss'],
  templateUrl: './vat-scheme-supporting-documents.component.html',
})
export class VatSchemeSupportingDocumentsComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      vtVatScheme: {
        title: 'Vat Scheme',
        type: 'html',

        editor: {
          type: 'custom',
          component: VatSchemeEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtVatScheme.schemeName;
        },
      },
      vtSupportingDocument: {
        title: 'Supporting Document',
        type: 'html',

        editor: {
          type: 'custom',
          component: VatSchemeSupportingDocumentEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell.vtSupportingDocument.description;
        },
      },
      documentRule: {
        title: 'Document Rule',
        type: 'string',
      },
      softRule: {
        title: 'Soft Rule',
        valuePrepareFunction: (date, cell) => {
          if (cell.softRule === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      comment: {
        title: 'Comment',
        type: 'string',
      },
      vtVatSchemePeriodDescription: {
        title: 'Product Status',
        type: 'html',

        editor: {
          type: 'custom',
          component: PeriodDescriptionEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          return cell;
        },
      },
    },
  };

  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 7500,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;

  constructor(private vatSchemeSupportingDocumentsService: VatSchemeSupportingDocumentsService,
              private toasterService: ToasterService) {
    this.service = vatSchemeSupportingDocumentsService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Added expense code ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not add expense code ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'Expense Types',
            body: 'Deleted expense code ' + entity.data.name + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'Expense Types',
            body: 'Could not delete expense code ' + entity.data.name,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Updated expense code ' + entity.data.name + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not update expense code ' + entity.data.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
