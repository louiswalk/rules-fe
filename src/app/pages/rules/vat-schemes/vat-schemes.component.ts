import {Component} from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table';
import {Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {VatSchemesService} from './vat-schemes.service';
import {CurrencyEditorComponent} from '../drop-down-components/currency-editor-component';
import {VatAuthorityEditorComponent} from '../drop-down-components/vat-authority-editor-component';
import {VatSchemeTypeEditorComponent} from '../drop-down-components/vat-scheme-type-editor-component';
import {InvoiceStatusEditorComponent} from '../drop-down-components/invoice-status-editor-component';
import {PeriodDescriptionEditorComponent} from '../drop-down-components/period-description-editor-component';
import {DuGroupEditorComponent} from '../drop-down-components/du-group-editor-component';
import {DuGroupSubmissionEditorComponent} from '../drop-down-components/du-group-submission-editor-component';
import {
  VatRuleLookupItemInvoiceDateCompareEditorComponent,
} from '../drop-down-components/vat-rule-lookup-item-invoice-date-compare-editor-component';
import {VatRuleLookupItemInvoiceUnitsEditorComponent} from '../drop-down-components/vat-rule-lookup-item-invoice-units-editor-component';
import {VatRuleLookupItemDeadlineUnitEditorComponent} from '../drop-down-components/vat-rule-lookup-item-deadline-unit-editor-component';


@Component({
  selector: 'ngx-vat-schemes',
  styleUrls: ['./vat-schemes.component.scss'],
  templateUrl: './vat-schemes.component.html',
})
export class VatSchemesComponent {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      schemeName: {
        title: 'Name',
        type: 'string',
      },
      agentRequired: {
        title: 'Agent required',
        valuePrepareFunction: (date, cell) => {
          if (cell.agentRequired === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      syCurrency: {
        title: 'Currency',
        type: 'html',
        editor: {
          type: 'custom',
          component: CurrencyEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.syCurrency)
            return cell.syCurrency.description;
        },
      },
      entityCodeVatAuthority: {
        title: 'Vat Authority',
        type: 'html',
        editor: {
          type: 'custom',
          component: VatAuthorityEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.syCurrency)
            return cell.syCurrency.description;
        },
      },
      scriptPrefix: {
        title: 'Prefix',
        type: 'string',
      },
      vtVatSchemeType: {
        title: 'Scheme Type',
        type: 'html',
        editor: {
          type: 'custom',
          component: VatSchemeTypeEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.vtVatSchemeType)
            return cell.vtVatSchemeType.description;
        },
      },
      invCapEnableInclusiveAmnt: {
        title: 'Cap Inclusive Amount',
        type: 'string',
        valuePrepareFunction: (date, cell) => {
          if (cell.invCapEnableInclusiveAmnt === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      invCapEnableExcllusiveAmnt: {
        title: 'Cap Exclusive Amount',
        type: 'string',
        valuePrepareFunction: (date, cell) => {
          if (cell.invCapEnableExcllusiveAmnt === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      quartlySubmission: {
        title: 'Quarterly',
        type: 'string',
        valuePrepareFunction: (date, cell) => {
          if (cell.quartlySubmission === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      quarterMinimum: {
        title: 'Quarter Minimum',
        type: 'string',
      },
      yearlySubmission: {
        title: 'Yearly',
        valuePrepareFunction: (date, cell) => {
          if (cell.quarterMinimum === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      yearlyMinimum: {
        title: 'Year Minimum',
        type: 'string',
      },
      imageForFinalise: {
        title: 'Image for Final',
        valuePrepareFunction: (date, cell) => {
          if (cell.imageForFinalise === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      imageForSubmission: {
        title: 'Image for Submission',
        valuePrepareFunction: (date, cell) => {
          if (cell.imageForSubmission === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      invoiceValidPeriod: {
        title: 'Valid Period',
        type: 'string',
      },
      invoicePeriodUnits: {
        title: 'Period Units',
        type: 'html',
        editor: {
          type: 'custom',
          component: VatRuleLookupItemInvoiceUnitsEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.invoicePeriodUnits)
            return cell.invoicePeriodUnits.description;
        },
      },
      invoiceDateCompare: {
        title: 'Date Compare',
        type: 'html',
        editor: {
          type: 'custom',
          component: VatRuleLookupItemInvoiceDateCompareEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.invoiceDateCompare)
            return cell.invoiceDateCompare.description;
        },
      },
      pastDateInvoiceStatus: {
        title: 'Past Invoice status',
        type: 'html',
        editor: {
          type: 'custom',
          component: InvoiceStatusEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.vtInvoiceStatus)
            return cell.vtInvoiceStatus.name;
        },
      },
      vtVatSchemePeriodDescription: {
        title: 'Period Description',
        type: 'html',

        editor: {
          type: 'custom',
          component: PeriodDescriptionEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.vtVatSchemePeriodDescription)
            return cell.vtVatSchemePeriodDescription.name;
        },
      },
      finaliseImageGrpId: {
        title: 'Image Final Group ID',
        type: 'html',

        editor: {
          type: 'custom',
          component: DuGroupEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.finaliseImageGrpId)
            return cell.finaliseImageGrpId.name;
        },
      },
      submissionImageGrpId: {
        title: 'Image Submission Group ID',
        type: 'html',

        editor: {
          type: 'custom',
          component: DuGroupSubmissionEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.submissionImageGrpId)
            return cell.submissionImageGrpId.name;
        },
      },
      invoiceRulesActive: {
        title: 'Rules Active',
        valuePrepareFunction: (date, cell) => {
          if (cell.invoiceRulesActive === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      claimSubmissionRulesActive: {
        title: 'Claim Rules Active',
        valuePrepareFunction: (date, cell) => {
          if (cell.claimSubmissionRulesActive === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      deadLineFromPeriodEnd: {
        title: 'Deadline from Period End',
        type: 'string',
      },
      deadLineFromPeriodEndUnit: {
        title: 'Deadline Unit',
        type: 'html',
        editor: {
          type: 'custom',
          component: VatRuleLookupItemDeadlineUnitEditorComponent,
        },
        valuePrepareFunction: (date, cell) => {
          if (cell.deadLineFromPeriodEndUnit)
            return cell.deadLineFromPeriodEndUnit.description;
        },
      },
      strictQuarters: {
        title: 'Strict Quarters',
        type: 'string',
      },
      strictAnnual: {
        title: 'String Annual',
        valuePrepareFunction: (date, cell) => {
          if (cell.strictAnnual === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      invCapEnableExemptAmnt: {
        title: 'Inv cap Exempt Vat',
        valuePrepareFunction: (date, cell) => {
          if (cell.invCapEnableExemptAmnt === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      invCapEnableNonVat: {
        title: 'Inv cap Non Vat',
        valuePrepareFunction: (date, cell) => {
          if (cell.invCapEnableNonVat === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      invoiceRulesOnClient: {
        title: 'Invoice Rules On Client',
        valuePrepareFunction: (date, cell) => {
          if (cell.invoiceRulesOnClient === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      clientPaidDirectly: {
        title: 'Client Paid Directly',
        valuePrepareFunction: (date, cell) => {
          if (cell.clientPaidDirectly === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      shortName: {
        title: 'Short Name',
        type: 'string',
      },
      quarterOverlap: {
        title: 'Quarter Overlap',
        type: 'string',
      },
      potentialDefault: {
        title: 'Potential Default',
        valuePrepareFunction: (date, cell) => {
          if (cell.potentialDefault === 'true') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
      region: {
        title: 'Region',
        type: 'string',
      },
      hasRegions: {
        title: 'Has Regions',
        valuePrepareFunction: (date, cell) => {
          if (cell.hasRegions === 'T') {
            return 'Yes';
          } else {
            return 'No';
          }
        },
        editor: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
          },
        },
      },
    },
  };

  config = new ToasterConfig({
    positionClass: 'toast-top-right',
    timeout: 7500,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 0,
  });

  source: LocalDataSource = new LocalDataSource();
  service: any;

  constructor(private vatSchemesService: VatSchemesService,
              private toasterService: ToasterService) {
    this.service = vatSchemesService;
    this.getEntities();
  }

  addEntity(entity): void {
    this.service.addEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Added expense code ' + entity.newData.name + ' successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not add expense code ' + entity.newData.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  deleteEntity(entity): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteEntity(entity.data).subscribe(
        onSuccess => {
          entity.confirm.resolve();
          const toast: Toast = {
            type: 'info',
            title: 'Expense Types',
            body: 'Deleted expense code ' + entity.data.name + ' successfully',
          };
          this.toasterService.popAsync(toast);
        },
        onFailure => {
          const toast: Toast = {
            type: 'error',
            title: 'Expense Types',
            body: 'Could not delete expense code ' + entity.data.name,
          };
          this.toasterService.popAsync(toast);
        },
      );
    } else {
    }
  }

  getEntities(): void {
    this.service.getEntities().subscribe(
      onSuccess => {
        this.source.load(onSuccess);
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Loaded successfully',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Loaded unsuccessfully ' + (onFailure.error.error ? onFailure.error.error : onFailure.message),
        };
        this.toasterService.popAsync(toast);
      },
    );
  }

  updateEntity(entity): void {
    this.service.updateEntity(entity.newData).subscribe(
      onSuccess => {
        entity.confirm.resolve();
        const toast: Toast = {
          type: 'info',
          title: 'Expense Types',
          body: 'Updated expense code ' + entity.data.name + ' successfully ',
        };
        this.toasterService.popAsync(toast);
      },
      onFailure => {
        entity.confirm.reject();
        const toast: Toast = {
          type: 'error',
          title: 'Expense Types',
          body: 'Could not update expense code ' + entity.data.name,
        };
        this.toasterService.popAsync(toast);
      },
    );
  }
}
