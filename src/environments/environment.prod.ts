/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  authUrl: 'https://auth.vatit.com/',
  authRedirectUrl: 'https://172.16.9.69/token-exchange',
  authLogoutRedirectUrl: 'https://172.16.9.69',
  authClientId: 'cn5PRM3UifkpvAVwGApGYNqKRVTXgjtM',
};
